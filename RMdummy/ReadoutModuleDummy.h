/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_DUMMY_H
#define READOUT_MODULE_DUMMY_H 

#include <string>
#include <vector>

#include "ipc/partition.h"
#include "ipc/core.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "RMdummy/dal/dummyNamed.h"
#include "RMdummy/DummySpyBuffer.h"
#include "RMdummy/dal/ReadoutModule_Dummy.h"
#include "ftkcommon/DataFlowReader.h"	//DataFlowReaderDummy
#include "RMdummy/DataFlowReaderDummyIS.h"	//DataFlowReaderDummyIS
#include "RMdummy/StatusRegisterDummyFactory.h"

#include "ftkcommon/ReadoutModule_FTK.h"

// EMon
#include "ftkcommon/FtkEMonDataOut.h"
#include <memory>			                //for shared pointers
#include <utility> 			              //for pairs

namespace daq {
namespace ftk {


  class ReadoutModule_Dummy : public ReadoutModule_FTK
  {
  public:

    /// Constructor (NB: executed at CONFIGURE transition)
    ReadoutModule_Dummy();
 
    /// Destructor (NB: executed at UNCONFIGURE transition)
    virtual ~ReadoutModule_Dummy() noexcept;
  
  public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin
    
    /// Set internal variables (NB: executed at CONFIGURE transition)
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;
    
    /// Reset internal statistics
    virtual void clearInfo() override;
   
    /// Get the list of channels connected to this ReadoutModule
    /// Used for monitoring. It starts at connect and gets data via VME
    virtual const std::vector<ROS::DataChannel *> *channels() override; 


  public: // overloaded methods inherited from RunControl::Controllable

    /// RC configure transition
    virtual void doConfigure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC connect transition
    virtual void doConnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC startNoDF subtransition
    virtual void startNoDF(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC checkConfig subtransition
    virtual void checkConfig(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC checkConnect subtransition
    virtual void checkConnect(const daq::rc::SubTransitionCmd& cmd) override;

    /// RC start of run transition
    virtual void doPrepareForRun(const daq::rc::TransitionCmd& cmd) override;
   

    /// RC stop transition
    virtual void doStopDC(const daq::rc::TransitionCmd& cmd) override;
    virtual void doStopROIB(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopHLT(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopRecording(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopGathering(const daq::rc::TransitionCmd& cmd) override {}
    virtual void doStopArchiving(const daq::rc::TransitionCmd& cmd) override {}
    
    /// RC unconfigure transition 
    virtual void doUnconfigure(const daq::rc::TransitionCmd& cmd) override;
    
    /// RC disconnect transition: 
    virtual void doDisconnect(const daq::rc::TransitionCmd& cmd) override;

    // Method called periodically by RC
    // if needed
    virtual void doPublish( uint32_t flag, bool finalPublish ) override;

    /// Method called periodically by RC
    /// Here the dummy spybuffer are sent to Emon via FTKEmonDataOut
    virtual void doPublishFullStats( uint32_t flag );

    /// RC HW re-synchronization request
    virtual void resynch(const daq::rc::ResynchCmd& cmd);

    /// Function that returns the name of the ReadoutModule
    std::string name_ftk(){return m_name;}; 

  private:

    std::vector<ROS::DataChannel *>     m_dataChannels;   
    DFCountedPointer<ROS::Config>       m_configuration;    // Configuration Object, Map Wrapper
    std::string 		                    m_isServerName;     // IS Server
		std::string													m_appName;
    IPCPartition 		                    m_ipcpartition;     // Partition 
    uint32_t    		                    m_runNumber;        // Run Number
    bool                                m_dryRun;           // Bypass VME calls
    OHRootProvider                      *m_ohProvider;      // Histogram provider  
    std::shared_ptr<OHRawProvider<>>    m_ohRawProvider;   	// Histogram provider used by DataFlowReader
    TH1F                                *m_histogram;       // Histogram
    std::string                         m_name;          
    uint32_t                            m_slot;
    uint32_t			                      m_SpyBufferSize;    ///< SpyBuffer size
    uint32_t                            m_variance;         ///< Variance of the spybuffer size
    uint32_t                            m_SpyBufferTime;    ///< SpyBuffer reading time (ms)
    uint32_t                            m_TimeVariance;     ///< Variance of the spybuffer reading time
    std::shared_ptr< DummySpyBuffer >   m_schpt_in;            ///< Shared pointer 
    std::shared_ptr< DummySpyBuffer >   m_schpt_out;
    bool                                m_configuration_done;
    bool                                m_mem_test;
    bool                                m_cpu_test;
// EMon
    daq::ftk::FtkEMonDataOut          *m_ftkemonDataOut;                                ///< Store pointer to EMonDataOut module
    DataFlowReaderDummyIS             *m_dfReader;
    StatusRegisterDummyFactory	      *m_srDummyFactory;


    dummyNamed                          *m_dummyNamed;      // Access IS via schema 


  };

  // The DataChannels are used to get samples while running. 
  // For example they can be used for monitoring porpuses
  inline const std::vector<ROS::DataChannel *> *ReadoutModule_Dummy::channels()
  {
    return &m_dataChannels;
  }

} // namespace ftk
} // namespace daq

#endif // READOUT_MODULE_DUMMY_H
 

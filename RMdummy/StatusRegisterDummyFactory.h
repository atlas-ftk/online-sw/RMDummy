#ifndef STATUSREGISTERDummyFACTORY_H
#define STATUSREGISTERDummyFACTORY_H

#include "ftkcommon/StatusRegister/StatusRegisterFactory.h"
#include "RMDummy/dal/StatusRegisterDummyStatusNamed.h"

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Reads out Registers from the Dummy board using StatusRegisterCollection objects and publishes the data to IS
 */
class StatusRegisterDummyFactory : public StatusRegisterFactory 
{
public:
	/*! \brief StatusRegisterDummyFactory class constructor
	 *   
	 * \param boardName The name of the board being monitored
	 * \param boardSlot The Dummy slot number the board occupies
	 * \param registersToRead A string telling the class which collections to create
	 * \param isServerName The name of the IS server to publish to
	 * \param IPCPartition The IPCPartition to be used for IS publishing
	 */
	StatusRegisterDummyFactory(
		const std::string& boardName,
		u_int boardSlot,
		const std::string& registersToRead,
		const std::string& isServerName,
		const IPCPartition& ipcPartition
	);

	virtual ~StatusRegisterDummyFactory();

protected:
	/*! \brief Creates a collection if the corresponding character was passed to the Factory constructor.
	 *
	 * \param IDChar The character which must be passed to the constructor in order for this group of registers to be read out
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param firstAddress The address of the first register to be read
	 * \param finalAddress The address of the final register to be read
	 * \param addrIncremenet The increment between neighbouring addresses to be read
	 * \param collectionName A descriptive name for the collection of registers
	 * \param collectionShortName A brief name for the collection of registers
	 * \param ISObjectVector The vector in the IS interface object that the register values will be stored in
	 * \param ISObjectInfoVector If IS interface object is provided, a vector containing firstAddress, finalAddress, addrIncremenet, selectorAddress will be stored.
	 * \param selectorAddress Selector address, default value is 0
 	 * \param readerAddress Reader access for selector, default value is 0 
	 * \param type StatuRegister type (not used as it is hardhoded in StatusRegisterDummyCollection::StatusRegisterDummyCollection())
	 * \param access StatuRegister access type 
	 */
	virtual void setupCollection(
		char IDChar,
		uint fpgaNum,
		uint firstAddress, 
		uint finalAddress,
		uint addrIncrement,
		std::string collectionName, 
		std::string collectionShortName,
		std::vector<uint>* ISObjectVector,
		std::vector<uint>* ISObjectInfoVector=NULL,
                uint selectorAddress = 0,
                uint readerAddress = 0,
		srType type = srType::srOther,
                srAccess access = srAccess::dummy
	);

	/*! \brief Freeze/unfreeze the board before readout (if needed).
	 *
	 * \param access StatuRegister access type
	 * \param fpgaNum The fpga number the registers are to be read from
	 * \param opt Control freeze (= true) or unfreeze (= false)
	 */
	void freeze(srAccess /*access*/ = srAccess::dummy, uint32_t /*fpgaNum*/ = 0, bool /*opt*/ = true) {};


protected:
	uint m_slot;
private:

};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERDummyFACTORY_H */

#ifndef Dummy_SPY_BUFFER
#define Dummy_SPY_BUFFER

/*
 *  Class to create and store dummy spybuffers data
 */

#include <string>
#include <vector>

//Including the base class
#include "ftkcommon/SpyBuffer.h"

namespace daq { 
  namespace ftk {

    
    /// @class dummy_spyBuffer create and store dummy spybuffer  
    class DummySpyBuffer : public daq::ftk::SpyBuffer
    {

      private:
      
        unsigned int m_SpyBufferSize;  ///< Size of the spybuffer
        unsigned int m_ID;             ///< ID of the spybuffer
        unsigned int m_msec;           ///< Time need to read the spybuffer
        unsigned int m_RMslot;         ///< Number of the ReadoutModule slot
        unsigned int m_offset;         ///< SpyBuffer last write address (prepended with dummies)

        void fillOffset( const unsigned int& offsetWordPrefix = 0xACDC );     ///< Fill the offset to be unwraped by the base class        

      public:
      
        DummySpyBuffer();
        virtual ~DummySpyBuffer();

        void resetSpyBuffer();                                                                 ///< Clear the spybuffer
        void setSpyBufferSize(unsigned int SpyBufferSize)  { m_SpyBufferSize = SpyBufferSize; }    ///< Set the size of the dummy spybuffers
        void setSpyBufferTime(unsigned int msec)  { m_msec = msec; }                               ///< Set the reading time of the spybuffers
        void setReadoutModuleSlot(uint32_t RMslot);                                                ///< Set the ReadoutModule slot name
        void readSpyBufferIn();                                                               ///< Method to fill the dummy spybuffer
        void readSpyBufferOut();
        void spyBufferSleep();
        void resetID();                                                                         ///< Method to reset the spybuffer ID
        void setSpyFreeze(const uint32_t& freeze) { m_spyStatus = (m_spyStatus & ~SPY_STATUS_FREEZE_MASK) + (freeze << 16); }

        unsigned int getSpyDimension() {return m_buffer.size();}                                 ///< @return dimension of the spybuffer

        //base classes
        virtual int readSpyStatusRegister( uint32_t& spyStatus );
        virtual int readSpyBuffer();
      
    };

  } // namespace ftk
} // namespace daq

#endif

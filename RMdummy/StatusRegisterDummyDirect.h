#ifndef STATUSREGISTERDummyDIRECT_H
#define STATUSREGISTERDummyDIRECT_H

#include "ftkcommon/StatusRegister/StatusRegister.h"

#include <iostream>

// Namespaces
namespace daq {
namespace ftk {

/*! \brief Accesses the Dummy and reads out a single register
 */
class StatusRegisterDummyDirect
: public StatusRegister 
{
public:
	/*! \brief Class constructor
	 * 
	 * \param slot The Dummy slot of the board to read the register from
	 * \param fpgaNum The FPGA number to read the register from
	 * \param registerName The name of the register
	 * \param registerAddress The address of the register
	 * \param type The type of register to be read (left as an input to make modification later on easier)
	 */
	StatusRegisterDummyDirect(
		uint slot,
		uint fpgaNum,
		std::string registerName,
		uint registerAddress,
		srType type
	);
	~StatusRegisterDummyDirect();

	/*! \brief Reads the variable at the address passed to the constructor
	 * The dummy register values are filled with registerAddress + 0x100*fpgaNum +  0x1000*slot 
	 */
	void readout();

private:
	uint m_tmp;

};

} // namespace ftk
} // namespace daq

#endif /* STATUSREGISTERDummyDIRECT_H */

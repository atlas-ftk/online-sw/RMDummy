#ifndef DATAFLOWREADERDF_H
#define DATAFLOWREADERDF_H

#include "ftkcommon/DataFlowReader.h"

#include <string>
#include <vector>


// Namespaces
namespace daq {
namespace ftk {

/*! \brief  DataFormatter implementation of DataFlowReader
*/
class DataFlowReaderDummyIS : public DataFlowReader
{
public:
    /*! \brief Constructor: sets the input scheme (IS inputs or vectors)
     *  \param ohRawProvider Pointer to initialized  OHRawProvider object
     *  \param forceIS Force the code to use IS as the source of information (default is 0)
     */
    DataFlowReaderDummyIS(std::shared_ptr<OHRawProvider<>> ohRawProvider, bool forceIS);

    void getDummy1(std::vector<int64_t>& srv);
    void getDummy2(std::vector<int64_t>& srv);
    void getDummy3(std::vector<int64_t>& srv);

    void publishExtraHistos(uint32_t option = 0);

private:
    std::vector<std::string> getISVectorNames();

private:

};

} // namespace ftk
} // namespace daq

#endif /* DATAFLOWREADERDF_H */

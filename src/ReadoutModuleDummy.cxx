/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

#include <iostream>
#include <stdlib.h>
#include <sys/syscall.h>

// OKS, RC and IS
#include "RMdummy/ReadoutModuleDummy.h"
#include "RMdummy/dal/ReadoutModule_Dummy.h"
#include "RMdummy/DummySpyBuffer.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"
#include "ftkcommon/Utils.h"		//getDFOHNameString


// Emon
#include "ftkcommon/FtkEMonDataOut.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include <memory>                           //shared_ptr
#include <utility>                          //pair
#include <ctime>
#include <malloc.h>


#define MAX_PRIME 200000
#define SOMESIZE (100*1024*1024) // 100MB

namespace daq
{

namespace ftk
{

ReadoutModule_Dummy::ReadoutModule_Dummy()
  : m_configuration(0),
    m_dryRun(false),
    m_configuration_done(false)
{
  #include "cmt/version.ixx"
  std::cout << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl;
  std::cout << cmtversion;

  if (daq::ftk::isLoadMode())
    { ERS_LOG("Loading mode"); }
    
  m_schpt_in  = std::make_shared<DummySpyBuffer>();    ///< Creation of the shared pointers for position in and out
  m_schpt_out = std::make_shared<DummySpyBuffer>();    
    
  ERS_LOG("ReadoutModule_Dummy::constructor: Done");
}


ReadoutModule_Dummy::~ReadoutModule_Dummy() noexcept
{
  ERS_LOG("ReadoutModule_Dummy::destructor: Start");
  delete m_ohProvider;
  delete m_dfReader;
  delete m_histogram;
  delete m_srDummyFactory;
  delete m_dummyNamed;
  ERS_LOG("ReadoutModule_Dummy::destructor: Done");
}


void ReadoutModule_Dummy::setup(DFCountedPointer<ROS::Config> configuration)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Setup"); // Create log counter

  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  m_appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );
 
  // Read some info from the DB

  const ftk::dal::ReadoutModule_Dummy * module_dal = conf->get<ftk::dal::ReadoutModule_Dummy>(configuration->getString("UID"));
  m_name	  = module_dal->UID();
  m_slot	  = module_dal->get_Slot();
  m_isServerName  = readOutConfig->get_ISServerName(); 
  m_dryRun        = module_dal->get_DryRun();
  m_variance      = module_dal->get_var();
  m_SpyBufferSize = module_dal->get_SpyBuffSize();
  m_TimeVariance  = module_dal->get_Timevar();
  m_SpyBufferTime = module_dal->get_SpyBuffTime();
  m_mem_test = module_dal->get_MemTest();
  m_cpu_test = module_dal->get_CpuTest();

  initialize( m_name, module_dal, BOARD_DUMMY );

  if(m_dryRun)
    { 
			daq::ftk::ftkException ex(ERS_HERE, name_ftk(), "ReadoutModule_Dummy.DryRun option set in DB, IPBus calls with be skipped!" );
			ers::warning(ex); 
		}

  //checks to avoid negative spybuffer "readout" time and size (which lead to large positive values in uint32_t representation)
  if( m_SpyBufferTime < m_TimeVariance/2) {
    ERS_LOG("SpyBufferTime must be at least half of TimeVariance. Set it to the half of TimeVariance");
    m_SpyBufferTime = m_TimeVariance/2;
  }
  if( m_SpyBufferSize < m_variance/2) {
    ERS_LOG("SpyBufferSize must be at least half of var. Set it to the half of var");
    m_SpyBufferSize = m_variance/2;
  }


  //Example on how to use and rise the FTKFwVersion Issue info
  std::string FPGA_name = "Dummy_FPGA";
  int FW_version = 1234;
  daq::ftk::FTKFwVersion fw1(ERS_HERE, m_name, FPGA_name, FW_version);
  ers::info(fw1); 

	//FTKExeption example
	std::stringstream message;
  message << "Message content..";
  daq::ftk::ftkException ex(ERS_HERE,name_ftk(), message.str());
  ers::info(ex);
	ers::warning(ex);
	//ers::error(ex);
	//ers::fatal(ex);

  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
  { 
  
    m_dummyNamed = new dummyNamed( m_ipcpartition, isProvider.c_str() );
  
  } 
  catch( ers::Issue &ex)
  {
    daq::ftk::ISException e(ERS_HERE, "Error while creating dummyNamed object", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  
  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try { 
    m_ohProvider = new OHRootProvider ( m_ipcpartition , OHServer, OHName ); 
    m_ohRawProvider = std::make_shared<OHRawProvider<>>( m_ipcpartition , OHServer, getDFOHNameString(m_name,"",-1,-1,"") );
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);
  }
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  m_dfReader = new DataFlowReaderDummyIS(m_ohRawProvider, true);
  m_dfReader->init(m_name);
  
  // Construct the histograms
  m_histogram = new TH1F("newHistogram","newRegHistogram",10,-50.5,50.5);

  tLog.end(ERS_HERE);  // End log conter
}


void ReadoutModule_Dummy::doConfigure(const daq::rc::TransitionCmd& cmd)
{ 
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());
  
  if (m_cpu_test){
    ftkTimeLog tLog1(ERS_HERE, name_ftk(), "CPU high intensity activities");
    unsigned long i, num, primes = 0;
    for (num = 1; num <= MAX_PRIME; ++num) {
        for (i = 2; (i <= num) && (num % i != 0); ++i);
        if (i == num)
            ++primes;
    }
    ERS_LOG("Prime: " << primes );
    tLog1.end(ERS_HERE);
  }

  if (m_mem_test){
    ftkTimeLog tLog1(ERS_HERE, name_ftk(), "Emulate data laoding");
    char* buffer;
    buffer = (char*) malloc(SOMESIZE);    //malloc 100MB
    for (int n=0; n<SOMESIZE; n++)
      buffer[n]=rand()%26+'a';
    sleep(10);
    free(buffer);
    tLog1.end(ERS_HERE);
  }
  stringstream o;
  pid_t tid = syscall(SYS_gettid);
  o << " slot: "<< m_slot <<"  tid: "<< tid;
  m_configuration_done = true;
  
  tLog.end(ERS_HERE);
}


void ReadoutModule_Dummy::doUnconfigure(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString());

  tLog.end(ERS_HERE);
}



void ReadoutModule_Dummy::doConnect(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter

  m_ftkemonDataOut = reinterpret_cast< daq::ftk::FtkEMonDataOut* >(ROS::DataOut::sampled());
   if(!m_ftkemonDataOut)
     {
       ERS_LOG("EMonDataOut plugin not found!");
       daq::ftk::ftkException ex(ERS_HERE, name_ftk(),"EMonDataOut plugin not found! SpyBuffer data will not be pushished in emon");
			 ers::warning(ex);
     }
   else
     {
       m_ftkemonDataOut->setScalingFactor(1);
       ERS_LOG("Found EMonDataOut plugin");
     }

  m_srDummyFactory = new StatusRegisterDummyFactory(m_name, m_slot, "123", m_isServerName, m_ipcpartition);
  //Set arbitrary prescale value
  m_srDummyFactory->setDefPrescale(5);

  tLog.end(ERS_HERE);  // End log conter
}



void ReadoutModule_Dummy::doDisconnect(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_Dummy::startNoDF(const daq::rc::SubTransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_Dummy::checkConfig(const daq::rc::SubTransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_Dummy::checkConnect(const daq::rc::SubTransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  tLog.end(ERS_HERE);  // End log conter
}


void ReadoutModule_Dummy::doPrepareForRun(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  //reset dummy spybuffer ID
  m_schpt_in->resetID();                                                                         
  m_schpt_out->resetID();

  tLog.end(ERS_HERE);  // End log conter
}


void ReadoutModule_Dummy::doStopDC(const daq::rc::TransitionCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_Dummy::doPublish( uint32_t flag, bool finalPublish )
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Publishing..." ); // Create log counter

  ERS_LOG("Publishing DataFlow: start");
  m_srDummyFactory->readout();
  //m_dfReader->readIS();
  m_dfReader->setDataAndInfo( m_srDummyFactory->getRegisterContents(), m_srDummyFactory->getMetaData() );
  m_dfReader->publishAllHistos();
  ERS_LOG("Publishing DataFlow: Done");

  //////////////
  /*stringstream o;
  pid_t tid = syscall(SYS_gettid);
  o << " slot: "<< m_slot <<"  tid: "<< tid;
  ERS_LOG( o.str());
  sleep(2);*/

  // Publish in IS with schema, i.e. fill ISInfo object
  // Example:
  m_dummyNamed->ExampleString = "Test";
  m_dummyNamed->ExampleU16    = 77;
  m_dummyNamed->ExampleS32    = rand()%100;
  try { m_dummyNamed->checkin();}
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }

  // Fill and publish histograms
  // Example:
  m_histogram->Fill( rand()%100 - 50 );
  try { m_ohProvider->publish( *m_histogram, "newRegHistogram", true ); }
  catch ( daq::oh::Exception & ex)
  { // Raise a warning or throw an exception. 
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one 
    ers::warning(e);  //or throw e;  
  }
  //////
  ///////
  tLog.end(ERS_HERE);  // End log conter
}


void ReadoutModule_Dummy::doPublishFullStats( uint32_t flag  )
{
  //not sure SB publishing actually works...
  //return;
  pid_t tid = syscall(SYS_gettid);
  ftkTimeLog tLog(ERS_HERE, name_ftk(), "Publishing Full Stats ..." ); // Create log counter
  
  ERS_LOG("Slot: "<< m_slot << " Thread tid: " << tid;);

  //clear the spybuffer
  m_schpt_in  ->  resetSpyBuffer();                                                                   
  m_schpt_out ->  resetSpyBuffer();

  //Setting the Spybuffer reading time
  uint32_t time = m_SpyBufferTime;
  uint32_t randT = (rand()% m_TimeVariance);
  if (m_TimeVariance != 0)             
    time = m_SpyBufferTime - m_TimeVariance/2 + randT;
  m_schpt_in   -> setSpyBufferTime( time );  
  m_schpt_out  -> setSpyBufferTime( time );
  ERS_LOG("Spybuffer reading time (ms) : " << time);


  //Setting the Spybuffer Size
  uint32_t size = m_SpyBufferSize;
  if (m_variance!=0)                                                                           
    size = m_SpyBufferSize - m_variance/2 + rand()% m_variance;
  m_schpt_in  -> setSpyBufferSize( size );
  m_schpt_out -> setSpyBufferSize( size );
  ERS_LOG("Spybuffer size: " << size);

  // Set SpyBuffer freeze reason
  m_schpt_in ->setSpyFreeze(0x33);
  m_schpt_out->setSpyFreeze(0xbb);

  //Setting the RMslot numbers
  m_schpt_in->setReadoutModuleSlot(m_slot);                                                            
  m_schpt_out->setReadoutModuleSlot(m_slot);

  // Filling the dummy spybuffers 
  m_schpt_in->readSpyBufferIn();                                                                   
  m_schpt_out->readSpyBufferOut();

  Position position_in= Position::IN;
  Position position_out= Position::OUT;

  // Creation of the Source ID
  uint32_t sourceid_in = encode_SourceIDSpyBuffer(BoardType::SSB, m_slot, m_slot, position_in);            
  uint32_t sourceid_out = encode_SourceIDSpyBuffer(BoardType::SSB, m_slot , m_slot, position_out);
  
    // print SID information
    //ERS_LOG("ID of the IN Spybuffer: dump -> \t " << dump_SourceIDSpyBuffer(sourceid_in));
    //ERS_LOG("ID of the OUT Spybuffer: dump -> \t " << dump_SourceIDSpyBuffer(sourceid_out));

    //ERS_LOG("ID of the IN Spybuffer:  " << sourceid_in);
    //ERS_LOG("ID of the OUT Spybuffer: " << sourceid_out);

  // Creation of the pair
  std::pair< std::shared_ptr<SpyBuffer>, uint32_t> pair2ship = make_pair(m_schpt_in, sourceid_in);      
  std::pair< std::shared_ptr<SpyBuffer>, uint32_t> pair2ship_out = make_pair(m_schpt_out, sourceid_out);
  
  std::vector< std::pair< std::shared_ptr<SpyBuffer>, uint32_t>> vect2ship;                           
  std::vector< std::pair< std::shared_ptr<SpyBuffer>, uint32_t>> vect2ship_out;
  
  // Filling the vector
  vect2ship.push_back(pair2ship);                                                                
  vect2ship_out.push_back(pair2ship_out);
  
  // Shipping the vector to Emon
  m_ftkemonDataOut->sendData(vect2ship);                                                        
  m_ftkemonDataOut->sendData(vect2ship_out);
  
  tLog.end(ERS_HERE);  // End log conter
}

void ReadoutModule_Dummy::resynch(const daq::rc::ResynchCmd& cmd)
{
  ftkTimeLog tLog(ERS_HERE, name_ftk(), cmd.toString()); // Create log counter
  // Fill ME
  tLog.end(ERS_HERE);  // End log conter
}
   


void ReadoutModule_Dummy::clearInfo() 
{
  ERS_LOG("Clear histograms and counters");

  // Reset histograms
  m_histogram->Reset();

  // Reset the IS counters
  m_dummyNamed->ExampleS32 = 0 ;
  m_dummyNamed->ExampleU16 = 0 ;
}




//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_Dummy();
}

ROS::ReadoutModule* createReadoutModule_Dummy()
{
  return (new ReadoutModule_Dummy());
}

}   //namespace ftk

}   //namespace daq

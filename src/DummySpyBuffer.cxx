// Functions used for spy buffer access using Dummy block transfer

#include "RMdummy/DummySpyBuffer.h"
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <time.h>

namespace daq { 
  namespace ftk {
    
    DummySpyBuffer::DummySpyBuffer() : m_ID(0x00000000), m_offset(0xA) {;}
    DummySpyBuffer::~DummySpyBuffer(){}

    void DummySpyBuffer::resetSpyBuffer()
    {
      m_buffer.clear();
    }

    void DummySpyBuffer::resetID()
    {
      m_ID = 0x00000000;
    }

    void DummySpyBuffer::spyBufferSleep()             //sleep simulating the spybuffer reading time 
    {
      struct timespec tim, tim2;
      tim.tv_sec = (m_msec/1000);
      tim.tv_nsec = ((m_msec%1000)*1000000);
      nanosleep(&tim, &tim2);
    }

    void DummySpyBuffer::setReadoutModuleSlot(uint32_t RMslot)          
    {
      m_RMslot = RMslot;
    }

    void DummySpyBuffer::readSpyBufferIn()             // read_spy_buffer method to fill the dummy spybuffer
    {
      spyBufferSleep();
      fillOffset();

      m_buffer.push_back(0xCACCACCA);
      m_buffer.push_back(0xDEADCAFE);
      m_buffer.push_back(0xCAFE0000 + m_RMslot);
      m_buffer.push_back(m_ID++);                      //ID of the buffer
      
      std::cout << "Spybuffer ID: " << m_ID << std::endl;
      
      for (uint32_t i=0; i<m_SpyBufferSize; i++)
      {
        m_buffer.push_back(0xDEAD0000 + i);           //buffer dummy data
      }

      //need to configure m_buffer
      readSpyStatusRegister(m_spyStatus);      
      //spyBufferSleep();

    }

    void DummySpyBuffer::readSpyBufferOut()             // read_spy_buffer method to fill the dummy spybuffer
    {
      spyBufferSleep();
      fillOffset();

      m_buffer.push_back(0xCACCACCA);
      m_buffer.push_back(0xB0CAF0CA);
      m_buffer.push_back(0xCAFE0000 + m_RMslot);
      m_buffer.push_back(m_ID++);                      //ID of the buffer

      std::cout << "Spybuffer ID: " << m_ID << std::endl;

      for (uint32_t i=0; i<m_SpyBufferSize; i++)
      {
        m_buffer.push_back(0xDEAD0000 + i);           //buffer dummy data
      }

      //need to configure m_buffer
      readSpyStatusRegister(m_spyStatus);      
      unwrapBuffer();
      //spyBufferSleep();

    }

    int DummySpyBuffer::readSpyBuffer()
    {
      readSpyBufferIn();
      readSpyBufferOut();
      return 0;
    }

    int DummySpyBuffer::readSpyStatusRegister( uint32_t& spyStatus ){
      int log2SpyDim = std::log2(getSpyDimension());
      spyStatus = (spyStatus & ~SPY_STATUS_SIZE_MASK) + (((log2SpyDim + 1) & 0xF) << 28);
      spyStatus = (spyStatus & ~SPY_STATUS_OVERFLOW_MASK) + (1 << 24);
      spyStatus = (spyStatus & ~SPY_STATUS_LAST_WRITTEN_ADDRESS_MASK) + ((m_offset-1) & 0xFFFF);
      return 0;
    }  


    //-----------------------------------------

    void DummySpyBuffer::fillOffset( const unsigned int& offsetWordPrefix ) {
      unsigned int shiftedOffsetWordPrefix = (offsetWordPrefix & 0xFFFF ) << 16;

      for(uint32_t i=0; i<m_offset; i++) {
        m_buffer.push_back(shiftedOffsetWordPrefix + i);
      }
    }

  } // namespace ftk
} // namespace daq

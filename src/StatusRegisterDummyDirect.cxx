// functions for StatusRegisterDummyDirect.h

#include "RMdummy/StatusRegisterDummyDirect.h"

namespace daq {
namespace ftk {

// Public Member Functions:
StatusRegisterDummyDirect::StatusRegisterDummyDirect(
	uint slot,
	uint fpgaNum,
	std::string registerName,
	uint registerAddress,
	srType type
) {
	m_name = registerName;
	m_nameShort = registerName;
	m_address = registerAddress + 100*fpgaNum +  1000*slot;
	m_type = type;

	// The DummyDirect class only allows access via dummy, so variables as set accordingly
	m_access = srAccess::dummy;
	m_tmp = 0;
}

StatusRegisterDummyDirect::~StatusRegisterDummyDirect() {
	ERS_INFO("Deleting StatusRegisterDummyDirect: address = "<<m_address);
}

void StatusRegisterDummyDirect::readout() {	
	m_tmp ++;
	m_value = m_address + m_tmp;
}

} // namespace ftk
} // namespace daq

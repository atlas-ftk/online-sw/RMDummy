#include "RMdummy/DataFlowReaderDummyIS.h"
#include "ftkcommon/ReadSRFromISVectors.h"

#include "ftkcommon/Utils.h"

//OnlineServices to check if running within a partition
#include <ipc/partition.h>
#include <ipc/core.h>


// ers
#include <ers/ers.h>
#include "ftkcommon/exceptions.h"



using namespace std;
using namespace daq::ftk;

/************************************************************/
DataFlowReaderDummyIS::DataFlowReaderDummyIS(std::shared_ptr<OHRawProvider<>> ohRawProvider, bool forceIS)
   : DataFlowReader(ohRawProvider, forceIS)
/************************************************************/
{
}

/************************************************************/
vector<string> DataFlowReaderDummyIS::getISVectorNames()
/************************************************************/
{
  vector<string> srv_names;

  srv_names.push_back("Dummy1_sr_v");
  srv_names.push_back("Dummy2_sr_v");
  srv_names.push_back("Dummy3_sr_v");

  return srv_names;
}


/************************************************************/
void DataFlowReaderDummyIS::getDummy1(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(11, 0) %100);
  srv.push_back(readRegister(12, 0) %100);
}
/************************************************************/
void DataFlowReaderDummyIS::getDummy2(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(30, 1) %100);
  srv.push_back(readRegister(31, 1) %100);
}
/************************************************************/
void DataFlowReaderDummyIS::getDummy3(vector<int64_t>& srv)
/************************************************************/
{
  srv.push_back(readRegister(31, 1) %100);
  srv.push_back(readRegister(32, 1) %100);
  srv.push_back(readRegister(33, 1) %100);
}

/************************************************************/
void DataFlowReaderDummyIS::publishExtraHistos(uint32_t option)
/************************************************************/
{
  std::vector<int64_t> srv;
  std::vector<float> srvf;

  srv.clear();
  getDummy1(srv);  
  publishSingleTH1F(srv,"Dummy1");

  srv.clear();
  getDummy2(srv);  
  publishSingleTH1F(srv,"Dummy2");

  srv.clear();
  getDummy3(srv);  
  publishSingleTH1F(srv,"Dummy3");
}


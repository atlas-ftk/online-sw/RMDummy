// functions for StatusRegisterDummyFactory.h

#include "RMdummy/StatusRegisterDummyFactory.h"
#include "RMdummy/StatusRegisterDummyDirect.h"

#include "ftkcommon/StatusRegister/StatusRegisterCollection.h"

#include "ftkcommon/exceptions.h"

namespace daq {
namespace ftk {

StatusRegisterDummyFactory::StatusRegisterDummyFactory(
	const std::string& boardName,
	u_int boardSlot,
	const std::string& registersToRead,
	const std::string& isServerName,
	const IPCPartition& ipcPartition
	) : StatusRegisterFactory (boardName, registersToRead, isServerName)
{
	// Store passed variables
	m_slot = boardSlot;
	// IS Publishing Setup
        std::unique_ptr<StatusRegisterDummyStatusNamed> m_srDummySNamed;
	try {        
		m_srDummySNamed = std::make_unique<StatusRegisterDummyStatusNamed>(ipcPartition, m_isProvider.c_str());
	} catch(ers::Issue &ex)  {
		ISException e(ERS_HERE, "Error while creating StatusRegisterDummyStatusNamed object", ex);  // Append the original exception (ex) to the new one 
		ers::warning(e);  //or throw e;
	}
	m_srDummySNamed->Name = m_name;
	m_srDummySNamed->Slot = m_slot;

	setupCollection('1',0,10,15,1,"D1","D1",&(m_srDummySNamed->Dummy1_sr_v), &(m_srDummySNamed->Dummy1_sr_v_info));
	setupCollection('2',1,30,32,1,"D2 Prescale","D2",&(m_srDummySNamed->Dummy2_sr_v), &(m_srDummySNamed->Dummy2_sr_v_info));
	setupCollection('3',1,31,40,1,"D3","D3",&(m_srDummySNamed->Dummy3_sr_v), &(m_srDummySNamed->Dummy3_sr_v_info));

	m_srxNamed = std::move(m_srDummySNamed);

}

StatusRegisterDummyFactory::~StatusRegisterDummyFactory() {
	// delete any objects on top of the base StatusRegisterFactory
	ERS_LOG("Deleting StatusRegisterDummyFactory");
}

// Protected Member Functions
void StatusRegisterDummyFactory::setupCollection(
	char IDChar,
	uint fpgaNum,
	uint firstAddress, 
	uint finalAddress,
	uint addrIncrement,
	std::string collectionName, 
	std::string collectionShortName,
	std::vector<uint>* ISObjectVector,
	std::vector<uint>* ISObjectInfoVector,
	uint selectorAddress,
	uint readerAddress,
	srType type ,
        srAccess access
	) 
{
	// Checks if the input string contains the identifying character for the collection
  if (std::string::npos != m_inputString.find_first_of(toupper(IDChar))) {
    // If it does, a collection for the corresponding set of registers is created
    std::unique_ptr<StatusRegisterCollection> src = std::make_unique<StatusRegisterCollection>(type, access);
    src->setName(collectionName);
    src->setNameShort(collectionShortName);
    for ( uint i = firstAddress; i < finalAddress; i+=addrIncrement ) {
      src->addStatusRegister(std::make_unique<StatusRegisterDummyDirect>(m_slot, fpgaNum, collectionName, i, type));
    }
    m_collections.push_back(std::move(src));
    m_isObjectVectors.push_back(ISObjectVector);
    if(ISObjectInfoVector){
      setupCollectionInfo(ISObjectInfoVector, firstAddress, finalAddress, addrIncrement, fpgaNum, selectorAddress, readerAddress);
    }
  }
}

} // namespace ftk
} // namespace daq

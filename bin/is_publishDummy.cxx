/*
    A simplification of publish_any.cc originally written by Serguei Kolos
    
    Developed my M. Lisovyi

*/

#include <iostream>
#include <memory>
#include <vector>
#include <map>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <is/infoT.h>
#include <is/infodictionary.h>

//do debug printouts
bool debug = false;


int main( int argc, char ** argv )
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }

    CmdArgStr partition_name	( 'p', "partition", "partition_name", "Partition name [default: ATLAS]" );
    CmdArgStr server_name	( 's', "server", "server_name", "OH (IS) server name [default: Histogramming]" );
    CmdArgSet degugMode		( 'd', "debug", "Debug printouts [default: Reduced verbosity]" );

                                                         
    CmdLine cmd( *argv, &partition_name, &server_name, &degugMode, NULL );
    CmdArgvIter arg_iter( --argc, ++argv );
    cmd.description( "Display FTK DataFlow overview per board" );
    
    // set default values
    partition_name = "ATLAS";
    server_name = "DF";

    // parge command-line agrs
    cmd.parse(arg_iter);

    debug = degugMode;

    std::string provider_name = std::string(server_name) + ".FTK.XYZ";
    std::cout<< "Provider name is: " << provider_name <<std::endl;
    
    IPCPartition partition = IPCPartition( std::string(partition_name) );
    ISInfoDictionary isd( partition );

    ISInfoFloat xyz(36.6);

    isd.checkin(provider_name, xyz);
}
